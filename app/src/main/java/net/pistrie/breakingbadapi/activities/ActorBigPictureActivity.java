package net.pistrie.breakingbadapi.activities;

import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.squareup.picasso.Picasso;

import net.pistrie.breakingbadapi.R;
import net.pistrie.breakingbadapi.data.ActorBigPictureAPITask;
import net.pistrie.breakingbadapi.domain.ActorBigPicture;

public class ActorBigPictureActivity extends AppCompatActivity implements ActorBigPictureAPITask.ActorBigPictureListener {

    private final String TAG = getClass().getSimpleName();

    private ImageView mImageView;

    // activity aanmaken
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.actor_big_picture);

        Log.d(TAG, "onCreate called");

        mImageView = findViewById(R.id.actor_big_picture);

        loadDetails();
    }

    // details van de aangeklikte character ophalen
    private void loadDetails() {
        Log.d(TAG, "loadDetails method called");
        Bundle b = getIntent().getExtras();
        String url = null;
        if (b != null) {
            url = b.getString("url");
        }

        String[] params = {
                url
        };
        new ActorBigPictureAPITask(this).execute(params);
    }

    // listener voor de activity
    @Override
    public void onActorPictureAvailable(ActorBigPicture actorBigPicture) {

        Log.d(TAG, "onActorBigPictureAvailable -> current actor picture " + actorBigPicture.getImageUrl());

        Picasso
                .get()
                .load(actorBigPicture.getImageUrl())
                .placeholder(R.drawable.progress_animation)
                .into(mImageView);
    }
}
