package net.pistrie.breakingbadapi.activities;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.squareup.picasso.Picasso;

import net.pistrie.breakingbadapi.R;
import net.pistrie.breakingbadapi.data.ActorDetailedAPITask;
import net.pistrie.breakingbadapi.domain.ActorDetailed;

import java.util.List;

public class ActorDetailedActivity extends AppCompatActivity implements ActorDetailedAPITask.ActorDetailedListener {

    private final String TAG = getClass().getSimpleName();

    private TextView mName;
    private TextView mNickname;
    private ImageView mImageView;
    private TextView mStatus;
    private TextView mBirthday;
    private TextView mOccupations;
    private TextView mSeasonAppearances;
    private String imageUrl;
    private TextView mQuotes;

//    private int occupations;
//    private int seasons;
//    private int quotes;

    ActorDetailed actor; // = new  ActorDetailed(null, null, null, null, null, null, null, null);

    private static final String NAME_KEY = "name";
    private static final String NICKNAME_KEY = "nickname";
    private static final String IMAGE_KEY = "image";
    private static final String STATUS_KEY = "status";
    private static final String BIRTHDAY_KEY = "birthday";
    private static final String OCCUPATIONS_KEY = "occupations";
    private static final String SEASONS_KEY = "seasons";
    private static final String QUOTES_KEY = "quotes";

    private static final String QUOTES_URL = "https://www.breakingbadapi.com/api/quotes";

    // huidige staat opslaan
    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putString(NAME_KEY, mName.getText().toString());
        outState.putString(NICKNAME_KEY, mNickname.getText().toString());
        outState.putString(IMAGE_KEY, imageUrl);
        outState.putString(STATUS_KEY, mStatus.getText().toString());
        outState.putString(BIRTHDAY_KEY, mBirthday.getText().toString());
        outState.putString(OCCUPATIONS_KEY, mOccupations.getText().toString());
        outState.putString(SEASONS_KEY, mSeasonAppearances.getText().toString());
        outState.putString(QUOTES_KEY, mQuotes.getText().toString());
    }

    // activity aanmaken
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(net.pistrie.breakingbadapi.R.layout.actor_details);

        Log.d(TAG, "onCreate called");

        // state herstellen als er een saved state is
        if (savedInstanceState != null && savedInstanceState.getString(NICKNAME_KEY) != null) {
            String debug =
                    savedInstanceState.getString(NAME_KEY) + "\n" +
                    savedInstanceState.getString(NICKNAME_KEY) + "\n" +
                    savedInstanceState.getString(IMAGE_KEY) + "\n" +
                    savedInstanceState.getString(STATUS_KEY) + "\n" +
                    savedInstanceState.getString(BIRTHDAY_KEY) + "\n" +
                    savedInstanceState.getString(OCCUPATIONS_KEY) + "\n" +
                    savedInstanceState.getString(SEASONS_KEY);
            Log.d(TAG, "saved actor:\n" + debug);

            mName = findViewById(R.id.actor_details_name);
            mNickname = findViewById(R.id.actor_details_nickname);
            mImageView = findViewById(R.id.actor_details_image);
            mStatus = findViewById(R.id.actor_details_status);
            mBirthday = findViewById(R.id.actor_details_birthday);
            mOccupations = findViewById(R.id.actor_details_occupations);
            mSeasonAppearances = findViewById(R.id.actor_details_season_appearances);
            mQuotes = findViewById(R.id.actor_details_quotes);

            mName.setText(savedInstanceState.getString(NAME_KEY));
            mNickname.setText(savedInstanceState.getString(NICKNAME_KEY));
            imageUrl = savedInstanceState.getString(IMAGE_KEY);
            Picasso
                    .get()
                    .load(this.imageUrl)
                    .resize(500, 0)
                    .placeholder(R.drawable.progress_animation)
                    .into(mImageView);
            mStatus.setText(savedInstanceState.getString(STATUS_KEY));
            mBirthday.setText(savedInstanceState.getString(BIRTHDAY_KEY));
            mOccupations.setText(savedInstanceState.getString(OCCUPATIONS_KEY));
            mSeasonAppearances.setText(savedInstanceState.getString(SEASONS_KEY));
            mQuotes.setText(savedInstanceState.getString(QUOTES_KEY));
        } else {
            Log.d(TAG, "calling loadDetails");
            loadDetails();
        }
    }

    // details van de aangeklikte character ophalen
    private void loadDetails() {
        Log.d(TAG, "loadDetails method called");
        Bundle b = getIntent().getExtras();
        String detailsUrl = null;
        if (b != null) {
            detailsUrl = b.getString("url");
        }

        String[] params = {
                detailsUrl,
                QUOTES_URL
        };
        // uitvoeren van de background task
        new ActorDetailedAPITask(this).execute(params);
    }

    // listener voor de activity
    @Override
    public void onActorDetailedAvailable(ActorDetailed actorDetailed) {
        actor = actorDetailed;

        Log.d(TAG, "onActorDetailedAvailable -> Current actor " + actorDetailed.getName());

        mName = findViewById(R.id.actor_details_name);
        mNickname = findViewById(R.id.actor_details_nickname);
        mImageView = findViewById(R.id.actor_details_image);
        mStatus = findViewById(R.id.actor_details_status);
        mBirthday = findViewById(R.id.actor_details_birthday);
        mOccupations = findViewById(R.id.actor_details_occupations);
        mSeasonAppearances = findViewById(R.id.actor_details_season_appearances);
        mQuotes = findViewById(R.id.actor_details_quotes);

        // kijken hoeveel newlines er moeten komen
        String[] occupations = actorDetailed.getOccupations();
        String[] seasons = actorDetailed.getSeasonAppearances();
        String[] quotes = actorDetailed.getQuotes();
        int occupationsNr = Integer.parseInt(occupations[0]);
        int seasonsNr = Integer.parseInt(seasons[0]);
        int quotesNr = Integer.parseInt(quotes[0]);

        // geen quotes? geen TextView
        if (Integer.parseInt(quotes[0]) == 0) {
            mQuotes.setVisibility(View.GONE);
        }

        mName.setText(getString(R.string.get_name, actorDetailed.getName()));
        mNickname.setText(getString(R.string.get_nickname, actorDetailed.getNickname()));
        this.imageUrl = actor.getImageUrl();
        Picasso
                .get()
                .load(this.imageUrl)
                .resize(500, 0)
                .placeholder(R.drawable.progress_animation)
                .into(mImageView);
        mStatus.setText(getString(R.string.get_status, actorDetailed.getStatus()));
        mBirthday.setText(getString(R.string.get_birthday, actorDetailed.getBirthday()));
        mOccupations.setText(getResources().getQuantityString(R.plurals.get_occupations, occupationsNr, occupations[1]));
        mSeasonAppearances.setText(getResources().getQuantityString(R.plurals.get_seasons, seasonsNr, seasons[1]));
        mQuotes.setText(getResources().getQuantityString(R.plurals.get_quotes, quotesNr, quotes[1]));
    }
}
