package net.pistrie.breakingbadapi.data;

import android.os.AsyncTask;
import android.util.Log;

import net.pistrie.breakingbadapi.activities.ActorDetailedActivity;
import net.pistrie.breakingbadapi.domain.ActorDetailed;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class ActorDetailedAPITask extends AsyncTask<String, Void, ActorDetailed> {

    private final String TAG = getClass().getSimpleName();
    private final static String JSON_ACTOR_ID = "char_id";
    private final static String JSON_ACTOR_NAME = "name";
    private final static String JSON_ACTOR_NICKNAME = "nickname";
    private final static String JSON_ACTOR_IMAGE = "img";
    private final static String JSON_ACTOR_STATUS = "status";
    private final static String JSON_ACTOR_BIRTHDAY = "birthday";
    private final static String JSON_ACTOR_OCCUPATION_ARRAY = "occupation";
    private final static String JSON_ACTOR_SEASONS_ARRAY = "appearance";
    private final static String JSON_QUOTE = "quote";
    private final static String JSON_QUOTE_AUTHOR = "author";

    private ActorDetailedListener listener = null;

    public ActorDetailedAPITask(ActorDetailedActivity listener) {
        this.listener = listener;
    }

    // uitvoeren van api request
    @Override
    protected ActorDetailed doInBackground(String... params) {

        // STAPPEN
        // URL maken/ophalen/samenstellen
        // request bouwen en versturen
        // response afwachten en verwerken
        // arraylist maken

        Log.d(TAG, "params index 0 -> " + params[0]);
        Log.d(TAG, "params index 1 -> " + params[1]);

        String actorUrlString = params[0];
        String actorQuotesUrlString = params[1];

        URL url = null;
        HttpURLConnection urlConnection = null;

        String detailsResponse;
        String quotesResponse;

        try {
            url = new URL(actorUrlString);

            urlConnection = (HttpURLConnection) url.openConnection();

            InputStream in = urlConnection.getInputStream();

            Scanner scanner = new Scanner(in);
            scanner.useDelimiter("\\A");

            boolean hasInput = scanner.hasNext();
            if (hasInput) {
                detailsResponse = scanner.next();

                Log.d(TAG, "detailsResponse -> " + detailsResponse);
            } else {
                return null;
            }

            url = new URL(actorQuotesUrlString);

            urlConnection = (HttpURLConnection) url.openConnection();

            in = urlConnection.getInputStream();

            scanner = new Scanner(in);
            scanner.useDelimiter("\\A");

            hasInput = scanner.hasNext();
            if (hasInput) {
                quotesResponse = scanner.next();

                Log.d(TAG, "quotesResponse -> " + quotesResponse);
            } else {
                return null;
            }
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        } finally {
            if (null != urlConnection) {
                urlConnection.disconnect();
            }
        }

        Log.d(TAG, "details: " + detailsResponse + "\nquotes: " + quotesResponse);
        return convertJsonToActorDetailed(detailsResponse, quotesResponse);
    }

    // teruggeven van de character
    @Override
    protected void onPostExecute(ActorDetailed actorDetailed) {
        super.onPostExecute(actorDetailed);

        Log.d(TAG, "in onPostExecute -> " + actorDetailed.getName());

        listener.onActorDetailedAvailable(actorDetailed);
    }

    // hulpmethode
    private ActorDetailed convertJsonToActorDetailed(String detailsResponse, String quotesResponse) {
        ActorDetailed actorDetailed = null;
        ArrayList<String> quotes = new ArrayList<>();

        // omzetten van String naar ArrayList
        try {
            // details api response
            JSONArray actorArray = new JSONArray(detailsResponse);
            JSONObject actor = actorArray.getJSONObject(0);
            JSONArray jsonOccupationsArray = actor.getJSONArray(JSON_ACTOR_OCCUPATION_ARRAY);
            JSONArray jsonSeasonsArray = actor.getJSONArray(JSON_ACTOR_SEASONS_ARRAY);

            String id = actor.getString(JSON_ACTOR_ID);
            String name = actor.getString(JSON_ACTOR_NAME);
            String nickname = actor.getString(JSON_ACTOR_NICKNAME);
            String image = actor.getString(JSON_ACTOR_IMAGE);
            String status = actor.getString(JSON_ACTOR_STATUS);
            String birthday = actor.getString(JSON_ACTOR_BIRTHDAY);
            String[] occupations = jsonArrayToStringArray(jsonOccupationsArray);
            int[] seasons = jsonArrayToIntArray(jsonSeasonsArray);

            // quotes api response
            JSONArray quotesArray = new JSONArray(quotesResponse);
            for (int i = 0; i < quotesArray.length(); i++) {
                JSONObject quote = quotesArray.getJSONObject(i);

                if (quote.getString(JSON_QUOTE_AUTHOR).equals(name)) {
                    quotes.add(quote.getString(JSON_QUOTE));
                }
            }
            actorDetailed = new ActorDetailed(id, name, nickname, image, status, birthday, occupations, seasons, quotes);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        String actorDebug =
                "returning actor details:\n" +
                        actorDetailed.getId() + "\n" +
                        actorDetailed.getName() + "\n" +
                        actorDetailed.getNickname()+ "\n" +
                        actorDetailed.getImageUrl()+ "\n" +
                        actorDetailed.getStatus()+ "\n" +
                        actorDetailed.getBirthday()+ "\n" +
                        actorDetailed.getOccupations()+ "\n" +
                        actorDetailed.getSeasonAppearances()+ "\n";
        Log.d(TAG, actorDebug);

        return actorDetailed;
    }

    // hulpmethode
    private String[] jsonArrayToStringArray(JSONArray jsonArray) {
        int arraySize = jsonArray.length();
        String[] stringArray = new String[arraySize];

        for (int i = 0; i < arraySize; i++) {
            try {
                stringArray[i] = (String) jsonArray.get(i);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        Log.d(TAG, "jsonArrayToString -> returning " + Arrays.toString(stringArray));

        return stringArray;
    }

    // hulpmethode
    private int[] jsonArrayToIntArray(JSONArray jsonArray) {
        int arraySize = jsonArray.length();
        int[] intArray = new int[arraySize];

        for (int i = 0; i < arraySize; i++) {
            try {
                intArray[i] = (Integer) jsonArray.get(i);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        Log.d(TAG, "jsonArrayToInt -> returning " + Arrays.toString(intArray));

        return intArray;
    }

    public interface ActorDetailedListener {
        void onActorDetailedAvailable(ActorDetailed actorDetailed);
    }
}
