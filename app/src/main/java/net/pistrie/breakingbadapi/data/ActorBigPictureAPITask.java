package net.pistrie.breakingbadapi.data;

import android.os.AsyncTask;
import android.util.Log;

import net.pistrie.breakingbadapi.domain.ActorBigPicture;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Scanner;

public class ActorBigPictureAPITask extends AsyncTask<String, Void, ActorBigPicture> {

    private final String TAG = getClass().getSimpleName();
    private final static String JSON_ACTOR_IMAGE = "img";

    private ActorBigPictureListener listener = null;

    public ActorBigPictureAPITask(ActorBigPictureListener listener) {
        this.listener = listener;
    }

    // uitvoeren van de api request
    @Override
    protected ActorBigPicture doInBackground(String... params) {

        Log.d(TAG, "params index 0 -> " + params[0]);
        String actorUrlString = params[0];

        URL url = null;
        HttpURLConnection urlConnection = null;

        try {
            url = new URL(actorUrlString);

            urlConnection = (HttpURLConnection) url.openConnection();

            InputStream in = urlConnection.getInputStream();

            Scanner scanner = new Scanner(in);
            scanner.useDelimiter("\\A");

            boolean hasInput = scanner.hasNext();
            if (hasInput) {
                String response = scanner.next();
                Log.d(TAG, "response -> " + response);

                return convertJsonToActorBigPicture(response);
            } else {
                return null;
            }

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (null != urlConnection) {
                urlConnection.disconnect();
            }
        }

        return null;
    }

    // teruggeven van de image url
    @Override
    protected void onPostExecute(ActorBigPicture actorBigPicture) {
        super.onPostExecute(actorBigPicture);

        Log.d(TAG, "in onPostExecute -> " + actorBigPicture.getImageUrl());

        listener.onActorPictureAvailable(actorBigPicture);
    }

    // hulpmethode
    private ActorBigPicture convertJsonToActorBigPicture(String response) {
        String image = null;

        try {
            JSONArray actorArray = new JSONArray(response);
            JSONObject actor = actorArray.getJSONObject(0);

            image = actor.getString(JSON_ACTOR_IMAGE);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.d(TAG, "image url -> " + image);

        return new ActorBigPicture(image);
    }

    public interface ActorBigPictureListener {
        void onActorPictureAvailable(ActorBigPicture actorBigPicture);
    }
}
