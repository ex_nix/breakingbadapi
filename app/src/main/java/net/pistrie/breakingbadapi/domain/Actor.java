package net.pistrie.breakingbadapi.domain;

import android.os.Parcel;
import android.os.Parcelable;

public class Actor implements Parcelable {

    // attributes
    private final String id;
    private final String name;
    private final String nickname;
    private final String status;
    private final String imageUrl;

    public Actor(String id, String name, String nickname, String status, String imageUrl) {
        this.id = id;
        this.name = name;
        this.nickname = nickname;
        this.status = status;
        this.imageUrl = imageUrl;
    }

    protected Actor(Parcel in) {
        id = in.readString();
        name = in.readString();
        nickname = in.readString();
        status = in.readString();
        imageUrl = in.readString();
    }

    public static final Creator<Actor> CREATOR = new Creator<Actor>() {
        @Override
        public Actor createFromParcel(Parcel in) {
            return new Actor(in);
        }

        @Override
        public Actor[] newArray(int size) {
            return new Actor[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        dest.writeString(nickname);
        dest.writeString(status);
        dest.writeString(imageUrl);
    }

    public String getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public String getNickname() {
        return this.nickname;
    }

    public String getStatus() {
        return this.status;
    }

    public String getImageUrl() {
        return this.imageUrl;
    }
}

